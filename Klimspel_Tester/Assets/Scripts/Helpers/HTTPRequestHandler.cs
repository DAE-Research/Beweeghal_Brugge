﻿/*
 * +--+--+--+--+--+--+--+--o--+--+--CLIMBING GAME--+--+--+--+--+--+--+--+--+--+--+
 * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--o--+--+--+--+--+--+--+--+
 * +--+--+--+--+--+--+--+--+--+- Howest DAE-Research -+--+--+--+--+--+--+--+--+--+
 * 
 * This class allows for sending http requests in an easy way with providing
 * a file, subfolders and arguments. 
 * A custom delegate can be provided aswell that will be called once the request 
 * return message is received.
 * 
 * +--+--+--+--+--+--+--+--o--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 * +--+--+--+--+--+--+--+--+--+--+--+--+--o--+--+--+--+--+--+--+--+--+--+--+--+--+
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System;

public class HTTPRequest
{
    public string File;
    public Dictionary<string, object> Data;
    public List<string> Directories;

    public HTTPRequest()
    {
        File = "";
        Data = new Dictionary<string, object>();
        Directories = new List<string>();
    }     
}

public class HTTPRequestHandler
{
    public string Server { get { return _server; } set { _server = value; } }
    string _server = "localhost";
    string _host { get { return "http://" + _server + "/"; } }
    WebClient _client = null;
    static string _user = "root";
    static string _pass = "";

    public delegate void ReturnDelegate(string returnValue);
    public event ReturnDelegate OnDataReceived;

    public HTTPRequestHandler() { }
    public HTTPRequestHandler(string server, string user, string pass)
    {
        _server = server;
        _user = user;
        _pass = pass;
    }

    public void SendHTTPRequest(HTTPRequest request, ReturnDelegate returnAction = null)
    {
        string url = _host;

        if (request.Directories != null && request.Directories.Count > 0)
        {
            for (int i = 0; i < request.Directories.Count; ++i)
            {
                url += request.Directories[i] + "/";
            }
        }

        if (request.File != null)
        {
            url += request.File;
        }

        if (request.Data != null && request.Data.Count > 0)
        {
            url += "?";

            List<string> dataPairs = new List<string>();

            foreach (var element in request.Data)
            {
                var valueString = element.Value.ToString();
                dataPairs.Add(element.Key + "=" + valueString);
            }

            url += string.Join("&", dataPairs.ToArray());
        }

        HTTPRequestAsync(url, returnAction);
    }

    void HTTPRequestAsync(string url, ReturnDelegate returnAction = null)
    {
        if (_client == null)
        {
            _client = new WebClient { Credentials = new NetworkCredential(_user, _pass) };
            _client.DownloadDataCompleted += DownloadComplete;
        }

        AutoResetEvent waiter = new AutoResetEvent(false);
        Uri uri = new Uri(url);
        _client.DownloadDataAsync(uri, waiter);
        //waiter.WaitOne();
    }

    void DownloadComplete(object o, DownloadDataCompletedEventArgs a)
    {
        Debug.Log("Error: " + a.Error);

        try
        {
            Debug.Log("Result: " + System.Text.Encoding.UTF8.GetString(a.Result));
        }
        catch
        {
            Debug.Log("Unable to print result from request");
        }
    }
}
