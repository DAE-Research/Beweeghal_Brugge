﻿/*
 * +--+--+--+--+--+--+--+--o--+--+--CLIMBING GAME--+--+--+--+--+--+--+--+--+--+--+
 * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--o--+--+--+--+--+--+--+--+
 * +--+--+--+--+--+--+--+--+--+- Howest DAE-Research -+--+--+--+--+--+--+--+--+--+
 * 
 * This script is used to handle scene independent logic f.e calling the SceneManager,
 * Starting the tracking with the camera.
 * 
 * +--+--+--+--+--+--+--+--o--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 * +--+--+--+--+--+--+--+--+--+--+--+--+--o--+--+--+--+--+--+--+--+--+--+--+--+--+
 */

using UnityEngine;
using Helpers;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager>
{
    public Texture2D CursorTexture;
    public static Camera TrackingCamera
    {
        get
        {
            if (_trackingCamera == null)
                _trackingCamera = GameObject.FindGameObjectWithTag("TrackingCamera").GetComponent<Camera>();

            return _trackingCamera;
        }
        private set { }
    }

    Scenes _scene = Scenes.MAIN_MENU;
    static Camera _trackingCamera;

    public enum Scenes
    {
        MAIN_MENU = 0,
        WALL_TEST = 1,
    }

    protected override void Awake()
    {
        base.Awake();
        DontDestroyOnLoad(gameObject);

        if(CursorTexture != null)
        {
            Cursor.SetCursor(CursorTexture, Vector2.zero, CursorMode.ForceSoftware);
        }

        SceneManager.sceneLoaded += OnSceneLoaded;

        VapixHandler.CheckVapixVersion();
    }

    public void Setup()
    {
        // Prepare projector, no longer used with Medialon(Ocular) in place
        //QLCPlusController.Instance.Setup();

        // Prepare tracking camera
        VapixHandler.LoadPreset("wallgame");
    }

    public void StartMenu()
    {
        SceneManager.LoadScene((int)Scenes.MAIN_MENU);
    }

    public void StartTest()
    {
        Setup();
        SceneManager.LoadScene((int)Scenes.WALL_TEST);
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.buildIndex == (int)Scenes.WALL_TEST)
        {
            // Start tracker
            if (!Tracker.Instance.IsTracking)
            {
                Tracker.Instance.StartTracking();
            }
        }
    }

    void Update()
    {
        // Input
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(_scene == Scenes.MAIN_MENU)
            {
                Application.Quit();
            }
            else
            {
                SceneManager.LoadScene((int)Scenes.MAIN_MENU);
            }
        }

        if(Input.GetKeyDown(KeyCode.I))
        {
            VapixHandler.CheckVapixVersion();
        }
    }
}
