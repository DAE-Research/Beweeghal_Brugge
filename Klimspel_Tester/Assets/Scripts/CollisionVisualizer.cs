﻿/*
 * +--+--+--+--+--+--+--+--o--+--+--CLIMBING GAME--+--+--+--+--+--+--+--+--+--+--+
 * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--o--+--+--+--+--+--+--+--+
 * +--+--+--+--+--+--+--+--+--+- Howest DAE-Research -+--+--+--+--+--+--+--+--+--+
 * 
 * This script allows to toggle a small window on the screen that shows 
 * the collision masks. This is useful to check if the camera stream is stable and
 * if the masks of game elements are aligned with the projection
 * 
 * +--+--+--+--+--+--+--+--o--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 * +--+--+--+--+--+--+--+--+--+--+--+--+--o--+--+--+--+--+--+--+--+--+--+--+--+--+
 */

using UnityEngine;

public class CollisionVisualizer : MonoBehaviour
{
    Camera _cam;
	
    void Awake()
    {
        _cam = GetComponent<Camera>();
        _cam.enabled = false;
    }

    void Update()
    {
        if(Input.GetKeyUp(KeyCode.V))
        {
            _cam.enabled = !_cam.enabled;
        }
    }
}
