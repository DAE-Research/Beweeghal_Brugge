﻿/*
 * +--+--+--+--+--+--+--+--o--+--+--CLIMBING GAME--+--+--+--+--+--+--+--+--+--+--+
 * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--o--+--+--+--+--+--+--+--+
 * +--+--+--+--+--+--+--+--+--+- Howest DAE-Research -+--+--+--+--+--+--+--+--+--+
 * 
 * This script uses images from the videograbber(see VapixHandler) and OpenCV to 
 * generate a mask of the player. The player mask can then be used to check for 
 * collision with masks of gameplay elements.
 * 
 * +--+--+--+--+--+--+--+--o--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 * +--+--+--+--+--+--+--+--+--+--+--+--+--o--+--+--+--+--+--+--+--+--+--+--+--+--+
 */

using UnityEngine;
using Helpers;
using System.Threading;
using OpenCVForUnity;
using UnityEngine.UI;

public class Tracker : Singleton<Tracker>
{
    public float FilterValue;
    public static float VerticalAdjustmentFactor = 0.1f;
    public Mat PlayerMask { get { return _maskMat; } private set { } }
    public bool IsTracking { get { return _bTracking; } private set { } }

    bool _bTracking;
    Mat _differenceMat;
    Mat _refMat;
    Mat _currentMat;
    Mat _maskMat;
    Mat _maskMatInverse;
    Texture2D _currentTex;
    Texture2D _refTex;

    protected override void Awake()
    {
        base.Awake();
        DontDestroyOnLoad(gameObject);
    }

    void OnDestroy()
    {
        VapixHandler.StopVideoGrabbing();
        Resources.UnloadUnusedAssets();
        //Thread.Sleep(1000);
    }

    public void StartTracking()
    {
        // Start video grabbing
        VapixHandler.StartVideoGrabbing();
        //Thread.Sleep(500);
        //SetReference();

        // Start Masking
        _bTracking = true;
    }

    public void UpdateVideo()
    {
        if (VapixHandler.FrameImage == null)
            return;

        if (_currentMat == null)
            _currentMat = new Mat(VapixHandler.FrameHeight, VapixHandler.FrameWidth, CvType.CV_8UC3);

        if (_currentMat.cols() != VapixHandler.FrameWidth || _currentMat.rows() != VapixHandler.FrameHeight)
            return;

        Utils.copyToMat(VapixHandler.FrameImage.Bytes, _currentMat);

        Resources.UnloadUnusedAssets();
    }

    public void SetReference()
    {
        Debug.Log("Tracker::Setting reference");
        UpdateVideo();

        if (_currentMat != null)
        {
            // release all
            _refTex = null;
            Resources.UnloadUnusedAssets();

            _refMat = _currentMat.clone();

            // create new tex
            _refTex = new Texture2D(_refMat.cols(), _refMat.rows());
            Utils.matToTexture2D(_refMat, _refTex);
        }
    }

    void UpdateMask()
    {
        if (_refMat != null && _currentMat != null)
        {
            // Check and create mat if neccesary
            if (_differenceMat == null)
                _differenceMat = new Mat(VapixHandler.FrameHeight, VapixHandler.FrameWidth, CvType.CV_8UC3);
            _differenceMat.setTo(new Scalar(0, 0, 0));

            if (_maskMat == null)
                _maskMat = new Mat(VapixHandler.FrameHeight, VapixHandler.FrameWidth, CvType.CV_8UC1);
            _maskMat.setTo(new Scalar(0, 0, 0));

            if (_maskMatInverse == null)
                _maskMatInverse = new Mat(VapixHandler.FrameHeight, VapixHandler.FrameWidth, CvType.CV_8UC1);
            _maskMatInverse.setTo(new Scalar(1, 1, 1));

            Mat greyBlurredCurrent = new Mat(VapixHandler.FrameHeight, VapixHandler.FrameWidth, CvType.CV_8UC1);
            Mat greyBlurredRef = new Mat(VapixHandler.FrameHeight, VapixHandler.FrameWidth, CvType.CV_8UC1);

            Imgproc.cvtColor(_currentMat, greyBlurredCurrent, Imgproc.COLOR_BGR2GRAY);
            Imgproc.GaussianBlur(greyBlurredCurrent, greyBlurredCurrent, new Size(11, 11), 0);

            Imgproc.cvtColor(_refMat, greyBlurredRef, Imgproc.COLOR_BGR2GRAY);
            Imgproc.GaussianBlur(greyBlurredRef, greyBlurredRef, new Size(11, 11), 0);

            // Subtract the images
            Core.absdiff(greyBlurredCurrent, greyBlurredRef, _differenceMat);

            // Dilate
            // http://stackoverflow.com/questions/5154282/dilate-erode-modify-kernel-option
            Mat kernelMat = new Mat(3, 3, CvType.CV_8U);
            kernelMat.setTo(new Scalar(0));
            Imgproc.dilate(_differenceMat, _differenceMat, kernelMat, new Point(-1, -1), 3);

            // Check the things that have no difference and set those to white (255) in inverse mat
            Core.inRange(_differenceMat, new Scalar(0, 0, 0), new Scalar(FilterValue, FilterValue, FilterValue), _maskMatInverse);

            // Invertttt dem bitches
            Core.bitwise_not(_maskMatInverse, _maskMat);

            Texture2D texture = new Texture2D(VapixHandler.FrameWidth, VapixHandler.FrameHeight);
            Utils.matToTexture2D(_maskMat, texture);

            var triggerMask = GameObject.Find("Mask_Player").GetComponent<Image>();
            Sprite sprite = Sprite.Create(texture, new UnityEngine.Rect(0, 0, VapixHandler.FrameWidth, VapixHandler.FrameHeight), new Vector2(0.5f, 0.5f));

            triggerMask.sprite = sprite;
        }
    }

    void Update()
    {
        if (!_bTracking || !VapixHandler.IsVideoGrabbing)
            return;

        // update the current frame's mat
        UpdateVideo();

        // mask it up
        UpdateMask();

        // input
        if (Input.GetKeyDown(KeyCode.F))
        {
            SetReference();
        }
    }
}
