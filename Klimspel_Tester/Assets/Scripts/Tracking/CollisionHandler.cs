﻿/*
 * +--+--+--+--+--+--+--+--o--+--+--CLIMBING GAME--+--+--+--+--+--+--+--+--+--+--+
 * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--o--+--+--+--+--+--+--+--+
 * +--+--+--+--+--+--+--+--+--+- Howest DAE-Research -+--+--+--+--+--+--+--+--+--+
 * 
 * This script handles the collision between the player mask and a mask of 
 * a game element.
 * Collision is checked by overlapping both maks using a bitwise and operation. 
 * Then the amount of overlapping pixels is compared to the amount of non-black 
 * pixels in the game element's mask. If the ratio is higher than the value of 
 * HitPercentageValue or the given requiredPercentage it counts as a collision.
 * 
 * +--+--+--+--+--+--+--+--o--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 * +--+--+--+--+--+--+--+--+--+--+--+--+--o--+--+--+--+--+--+--+--+--+--+--+--+--+
 */

using UnityEngine;
using Helpers;
using OpenCVForUnity;

public class CollisionHandler : Singleton<CollisionHandler>
{
    Mat _collisionMat = null;
    public int HitPercentageValue;

    protected override void Awake()
    {
        base.Awake();
        DontDestroyOnLoad(gameObject);
    }

    public bool CheckPlayerCollision(Mat _mat, int requiredPercentage = -1)
    {
        // Retrieve the player mask
        Mat playerMask = Tracker.Instance.PlayerMask;

        // Check if both masks are valid
        if (playerMask == null)
        {
            Debug.LogError("CollisionHandler::playerMask is null! Player: " + playerMask + ", Mat: " + _mat);
            return false;
        }

        if (_mat == null)
        {
            Debug.LogError("InputMask is null! Player: " + playerMask + ", Mat: " + _mat);
            return false;
        }

        var handlesPixels = 0;
        var collisionPixels = 0;
        // Count the game element's mask amount of non-black pixels
        handlesPixels = Core.countNonZero(_mat);

        if (_collisionMat == null)
            _collisionMat = new Mat(VapixHandler.FrameHeight, VapixHandler.FrameWidth, CvType.CV_8U);

        _collisionMat.setTo(new Scalar(0));
        // Perform bitwise and operation on both Masks to get the overlapping pixels
        Core.bitwise_and(playerMask, _mat, _collisionMat);
        collisionPixels = Core.countNonZero(_collisionMat);

        // Check if the ratio of overlapping pixels is higher than HitPercentageValue or requiredPercentage
        var percentage = HitPercentageValue;
        if (requiredPercentage != -1)
            percentage = requiredPercentage;
        float thresholdPixelCount = (handlesPixels * (percentage / 100.0f));
        if (thresholdPixelCount < collisionPixels)
        {
            Debug.Log("CollisionHandler::Collision!");
            return true;
        }
        
        return false;
    }
}
