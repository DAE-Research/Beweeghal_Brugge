﻿/*
 * +--+--+--+--+--+--+--+--o--+--+--CLIMBING GAME--+--+--+--+--+--+--+--+--+--+--+
 * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--o--+--+--+--+--+--+--+--+
 * +--+--+--+--+--+--+--+--+--+- Howest DAE-Research -+--+--+--+--+--+--+--+--+--+
 * 
 * This script handles the communication with the axis camera's.
 * Use the basic info functions to get information about the camera and its 
 * functionality.
 * Use the PTZ (Pan Tilt Zoom) functions to control the camera movement and zooming.
 * Use the video grabbing functions to retrieve the video stream. 
 * 
 * +--+--+--+--+--+--+--+--o--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 * +--+--+--+--+--+--+--+--+--+--+--+--+--o--+--+--+--+--+--+--+--+--+--+--+--+--+
 */          

using UnityEngine;
using System.Collections.Generic;
using Emgu.CV;
using Emgu.CV.Structure;
using System.Net;
using System.Threading;
using System;
using System.Linq;
using OpenCVForUnity;
using System.IO;

public static class VapixHandler
{
    public static int FrameWidth { get { return _frameWidth; } private set { } }
    public static int FrameHeight { get { return _frameHeight; } private set { } }

    public delegate void ReturnDelegate(string returnValue);
    public static event ReturnDelegate OnDataReceived;

    public delegate void TextureLoaded();
    public static event TextureLoaded OnTextureLoaded;

    public static Texture2D Texture { get { return _texture; } private set { } } // Loaded images will be stored in this texture
    public static bool IsVideoGrabbing { get { return _bVideoGrabbing; } private set { } } 
    public static Queue<byte[]> VideoBuffer { get { return _videoBuffer; } private set { } }
    public static volatile int FrameCount = 0;
    public static Image<Bgr, Byte> FrameImage { get { return _image; } private set { } }

    static List<string> _cameraAdresses = new List<string>()
    {
        "172.21.224.26",
        "172.21.224.27",
        "172.21.224.28",
        "172.21.225.10"
    };

    static int _camera = 2;
    static string _server = "172.21.224.28";
    static string _user = "root";
    static string _pass = "ocular16";
    static string _host { get { return "http://" + _server + "/axis-cgi/"; } }
    //1920x1080,1280x720,800x450,480x270,320x180
    static int _byteBufferSize = 170;//32768;//170
    static int _frameWidth = 800;//640;//320;//1920;
    static int _frameHeight = 450;//480;//240;//1080;
    static int _compression = 25;
    static Texture2D _texture = new Texture2D(800, 450);
    static volatile bool _bVideoGrabbing = false;
    static Thread _videoThread;
    static Stream _videoStream;
    static volatile WebClient _client = null;
    static Queue<byte[]> _videoBuffer;
    static volatile byte[] _videoFrame;
    static Texture2D _videoTexture = new Texture2D(_frameWidth, _frameHeight);
    static Emgu.CV.Capture _capture;
    static Image<Bgr, Byte> _image;
    static HTTPRequestHandler _httpRequestHandler = new HTTPRequestHandler(_server+"/axis-cgi", _user, _pass);

    public static void ClearCredentials()
    {
        _user = "";
        _pass = "";
    }

    public static void NextCamera()
    {
        var bWasVideoGrabbing = _bVideoGrabbing;
        if (_bVideoGrabbing)
        {
            _bVideoGrabbing = false;
        }

        if (_client != null)
        {
            _client.Dispose();
            _client = null;
        }

        ++_camera;
        _camera = _camera % _cameraAdresses.Count;
        SetServer(_cameraAdresses[_camera]);

        if(bWasVideoGrabbing)
        {
            StartVideoGrabbing();
        }
    }

    public static void SetServer(string server)
    {
        _server = server;
        _httpRequestHandler.Server = server;
    }

    public static void Clean()
    {
        Debug.Log("VapixHandler -- Cleanup");

        if(_bVideoGrabbing)
        {
            _bVideoGrabbing = false;
            if(_videoStream != null)
                _videoStream.Close();
            _videoThread.Join();
            _videoThread.Abort();
        }

        if(_videoThread != null)
        {
            _videoThread.Join();
            _videoThread.Abort();
        }

        if(_client != null)
        {
            _client.Dispose();
            _client = null;
        }
    }

    /*********************************** BASIC INFO ***************************************/
    #region Vapix basic
    public static void CheckVapixVersion()
    {
        HTTPRequest request = new HTTPRequest();
        request.File = "param.cgi";
        request.Data.Add("action", "list");
        request.Data.Add("group", "Properties.API.HTTP.Version");
        _httpRequestHandler.SendHTTPRequest(request);
    }

    public static void CheckSupportedResolutions()
    {
        HTTPRequest request = new HTTPRequest();
        request.File = "param.cgi";
        request.Data.Add("action", "list");
        request.Data.Add("group", "Properties.Image.Resolution");
        _httpRequestHandler.SendHTTPRequest(request);
    }

    public static void CheckSupportedImageFormats()
    {
        HTTPRequest request = new HTTPRequest();
        request.File = "param.cgi";
        request.Data.Add("action", "list");
        request.Data.Add("group", "Properties.Image.Format");
        _httpRequestHandler.SendHTTPRequest(request);
    }

    public static void GetCameraResolution(int camera)
    {
        HTTPRequest request = new HTTPRequest();
        request.File = "imagesize.cgi";
        request.Data.Add("camera", camera);
        _httpRequestHandler.SendHTTPRequest(request);
    }

    public static void GetVideoStatus(int source)
    {
        HTTPRequest request = new HTTPRequest();
        request.File = "videostatus.cgi";
        request.Data.Add("status", source);
        _httpRequestHandler.SendHTTPRequest(request);
    }

    public static void GetBitmap()
    {
        GetBitmap(-1, null);
    }

    public static void GetBitmap(int camera, string resolution = null)
    {
        HTTPRequest request = new HTTPRequest();
        request.File = "image.bmp";
        request.Directories.Add("bitmap");

        if(camera == -1 && resolution == null)
        {
            _httpRequestHandler.SendHTTPRequest(request);
        }
        else
        {
            if(camera != -1)
            {
                request.Data.Add("camera", camera);
            }
            if (resolution != null)
            {
                request.Data.Add("resolution", resolution);
            }

            _httpRequestHandler.SendHTTPRequest(request);
        }
    }

    public static void GetJPEG()
    {
        GetJPEG(-1);
    }

    public static void GetJPEG(int camera, string resolution = null, int compression = 0)
    {
        HTTPRequest request = new HTTPRequest();
        request.File = "image.cgi";
        request.Directories.Add("jpg");

        if (camera == -1 && resolution == null && compression == 0)
        {
            _httpRequestHandler.SendHTTPRequest(request);
        }
        else
        {
            var data = new Dictionary<string, object>();
            if (camera != -1)
            {
                request.Data.Add("camera", camera);
            }
            if (resolution != null)
            {
                request.Data.Add("resolution", resolution);
            }
            if (compression != 0)
            {
                request.Data.Add("compression", compression);
            }

            _httpRequestHandler.SendHTTPRequest(request);
        }
    }
    #endregion

    /************************************** PTZ *******************************************/
    #region PTZ
    public static void CheckPTZ()
    {
        HTTPRequest request = new HTTPRequest();
        request.File = "param.cgi";
        request.Data.Add("action", "list");
        request.Data.Add("group", "Properties.PTZ.PTZ");
        _httpRequestHandler.SendHTTPRequest(request);
    }

    public static void GetPTZInfo(int camera)
    {
        HTTPRequest request = new HTTPRequest();
        request.Directories.Add("com");
        request.File = "ptz.cgi";
        request.Data.Add("info", "1");
        request.Data.Add("camera", camera);
        _httpRequestHandler.SendHTTPRequest(request);
    }

    public static void SetPreset(string name)
    {
        HTTPRequest request = new HTTPRequest();
        request.Directories.Add("com");
        request.File = "ptzconfig.cgi";
        request.Data.Add("setserverpresetname", name);
        request.Data.Add("home", "yes");
        _httpRequestHandler.SendHTTPRequest(request);
    }

    public static void LoadPreset(string name)
    {
        HTTPRequest request = new HTTPRequest();
        request.Directories.Add("com");
        request.File = "ptz.cgi";
        request.Data.Add("gotoserverpresetname", name);
        _httpRequestHandler.SendHTTPRequest(request);
    }

    public static void PanCamera(int camera, int degrees)
    {
        HTTPRequest request = new HTTPRequest();
        request.Directories.Add("com");
        request.File = "ptz.cgi";
        request.Data.Add("rpan", degrees);
        request.Data.Add("camera", camera);
        _httpRequestHandler.SendHTTPRequest(request);
    }

    public static void SetCameraPan(int camera, int degrees)
    {
        HTTPRequest request = new HTTPRequest();
        request.Directories.Add("com");
        request.File = "ptz.cgi";
        request.Data.Add("pan", degrees);
        request.Data.Add("camera", camera);
        _httpRequestHandler.SendHTTPRequest(request);
    }

    public static void TiltCamera(int camera, int degrees)
    {
        HTTPRequest request = new HTTPRequest();
        request.Directories.Add("com");
        request.File = "ptz.cgi";
        request.Data.Add("rtilt", degrees);
        request.Data.Add("camera", camera);
        _httpRequestHandler.SendHTTPRequest(request);
    }

    public static void SetCameraTilt(int camera, int degrees)
    {
        HTTPRequest request = new HTTPRequest();
        request.Directories.Add("com");
        request.File = "ptz.cgi";
        request.Data.Add("tilt", degrees);
        request.Data.Add("camera", camera);
        _httpRequestHandler.SendHTTPRequest(request);
    }

    public static void SetCameraZoom(int camera, int value)
    {
        HTTPRequest request = new HTTPRequest();
        request.Directories.Add("com");
        request.File = "ptz.cgi";
        request.Data.Add("zoom", value);
        request.Data.Add("camera", camera);
        _httpRequestHandler.SendHTTPRequest(request);
    }
    #endregion

    /********************************* VIDEO GRABBING *************************************/
    #region Video grabbing
    public static void StartVideoGrabbing()
    {
        if(_videoThread != null)
        {
            _videoThread.Join();
        }

        _videoThread = new Thread(GrabVideoStreamRTSP);
        _videoThread.Start();

        //GrabVideoStreamRTSP();
    }

    public static void StopVideoGrabbing()
    {
        if(_videoThread != null)
        {
            _bVideoGrabbing = false;
            _videoThread.Join();
            _videoThread.Abort();
        }

        if(FrameImage != null)
        {
            FrameImage.Dispose();
        }
    }

    [Obsolete]
    public static Texture2D GetVideoFrameAsTexture2D()
    {
        if(_videoBuffer != null && _videoBuffer.Count > 0)
        {
            if (_videoBuffer.Count > 1)
            {
                for (int i = 0; i < _videoBuffer.Count - 1; ++i)
                {
                    _videoBuffer.Dequeue();
                }
            }

            _videoTexture.LoadImage(_videoBuffer.Peek());

            return _videoTexture;
        }
        else
        {
            return null;
        }
    }

    public static Mat GetVideoFrameAsMat()
    {
        Mat mat = new Mat(FrameHeight, FrameWidth, CvType.CV_8UC3);
        Utils.copyToMat(FrameImage.Bytes, mat);
        return mat;
    }

    static void GrabVideoStreamRTSP()
    {
        //var url = "http://" + _user + ":" + _pass + "@" + _server + "/axis-media/media.amp";
        //var url = "rtsp://" + _server + "/axis-media/media.amp";
        var url = "rtsp://" + _user + ":" + _pass + "@" + _server + "/axis-media/media.amp?videocodec=h264&resolution=" + _frameWidth + "x" + _frameHeight + "&fps=24";

        if (_capture != null)
            _capture.Dispose();

        try
        {
            _capture = new Emgu.CV.Capture(url);
        }
        catch (Exception e)
        {
            Debug.LogError("VapixHandler -- couldn't open stream " + e.Message);
            return;
        }

        if (_capture == null)
        {
            Debug.LogError("VapixHandler -- stream is null");
            return;
        }
        else
        {
            Debug.Log("VapixHandler -- stream is open");
            //return;
        }

        _bVideoGrabbing = true;
        while (_bVideoGrabbing)
        {
            _image = _capture.QueryFrame();
            if (_image == null)
                break;
        }

        if (_capture != null)
        {
            _capture.Dispose();
        }
    }

    [Obsolete]
    static void GrabVideoStreamHTTP()
    {
        /*
        http://stackoverflow.com/questions/21702477/how-to-parse-mjpeg-http-stream-from-ip-camera
        http://stackoverflow.com/questions/5867227/convert-streamreader-to-byte
        https://msdn.microsoft.com/en-us/library/781fwaz8(v=vs.110).aspx
        http://stackoverflow.com/questions/943635/getting-a-sub-array-from-an-existing-array
        */

        _bVideoGrabbing = true;

        if (_client == null)
        {
            _client = new WebClient { Credentials = new NetworkCredential(_user, _pass) };
        }
        else
        {
            //_client.Dispose();
            //_client = new WebClient { Credentials = new NetworkCredential(_user, _pass) };
        }
        //Thread.Sleep(500);
        //var url = "rtsp://172.21.224.28/axis-media/media.amp?videocodec=h264&resolution=800x480";
        var url = _host + "mjpg/video.cgi?" + "resolution=" + _frameWidth + "x" + _frameHeight + "&fps=" + 24 + "&compression=" + _compression;
        _videoBuffer = new Queue<byte[]>();
        _videoStream = _client.OpenRead(url);

        List<byte> bytes = new List<byte>();
        byte[] buffer = new byte[_byteBufferSize];
        byte[] imgStart = StringToByteArray("FFD8");
        byte[] imgEnd = StringToByteArray("FFD9");
        var startIndex = -1;
        var newendIndex = -1;

        while (_bVideoGrabbing)
        {
            

            //var streamLength = stream.Length;
            //Debug.Log("stream size: " + stream.Length);
            _videoStream.Read(buffer, 0, _byteBufferSize);
            _videoStream.Flush();
            bytes.AddRange(buffer);
            //Debug.Log(bytes.Count);
            //var byteArray = bytes.ToArray();

            //Only need to look for startIndex until it is found, since it wont change 
            if (startIndex == -1)
            {
                var byteArray = bytes.ToArray();
                if(byteArray == null || byteArray.Length == 0)
                {
                    //Debug.Log("invalid byteArray");
                    continue;
                }
                startIndex = SearchBytes(byteArray, imgStart);
                if (startIndex == -1)
                    continue;

                //by knowing the startIndex, the endIndex can be found by using the frame size provided in the header before the startIndex
                var headerBytes = byteArray.SubArray(startIndex-65, 65);
                var header = System.Text.Encoding.Default.GetString(headerBytes);
                //Debug.Log("header: " + header);
                if (!header.Contains("Content-Length"))
                {
                   // Debug.Log("no valid header");
                    startIndex = -1;
                    bytes.Clear();
                    continue;
                }
                else
                {
                    var frameSize = int.Parse(header.Split(' ').Last()); //quickanddirty
                    //Debug.Log("frameSize: " + frameSize);
                    newendIndex = startIndex + frameSize;
                }
            }

            if(bytes.Count > newendIndex + _byteBufferSize)
            {
                var byteArray = bytes.ToArray();

                //Sanity check
                string endBytesAsString = BitConverter.ToString(byteArray, newendIndex - 2, 2);
                if(endBytesAsString != "FF-D9")
                {

                    //string endBytes = BitConverter.ToString(byteArray, newendIndex - 4, 4);

                    //Debug.Log("endBytes were incorrect!");
                    var hardFindEndBytes = SearchBytes(byteArray, imgEnd, startIndex);
                    if (hardFindEndBytes != -1)
                    {
                        //Debug.Log("found endbytes on " + hardFindEndBytes + " vs " + newendIndex);
                        var hardFindStartBytes = SearchBytes(byteArray, imgStart, 0);
                        
                        startIndex = -1;
                        hardFindEndBytes += 2;
                        var restArray = byteArray.SubArray(hardFindEndBytes, byteArray.Length - hardFindEndBytes);
                        bytes.Clear();
                        bytes = restArray.ToList();
                        continue;
                    }
                    else
                    {
                        //Thread.Sleep(300);
                        startIndex = -1;
                        bytes.Clear();
                        continue;
                    }
                }

                _videoFrame = byteArray.SubArray(startIndex, newendIndex - startIndex);
                _videoBuffer.Enqueue(_videoFrame);
                //var chunk = byteArray.SubArray(0, newendIndex);
                //fs.Write(chunk, fsOffset, chunk.Length);
                //fsOffset += chunk.Length;
                bytes.Clear();
                var restBytes = byteArray.SubArray(newendIndex, (byteArray.Length - newendIndex));
                bytes = restBytes.ToList();

                ++FrameCount;

                startIndex = -1;
                newendIndex = -1;
            }
        }

        _videoStream.Close();
        //_client.Dispose();
        //_client = null;
        Debug.Log("video stream closed");
        //fs.Close();
    }

    #endregion

    /********************************* HELP FUNCTIONS *************************************/
    #region Help functions
    static byte[] StringToByteArray(String hex)
    {
        int NumberChars = hex.Length;
        byte[] bytes = new byte[NumberChars / 2];
        for (int i = 0; i < NumberChars; i += 2)
            bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
        return bytes;
    }

    static int SearchBytes(byte[] haystack, byte[] needle, int start = 0)
    {
        var len = needle.Length;
        var limit = haystack.Length - len;
        for (var i = start; i <= limit; i++)
        {
            var k = 0;
            for (; k < len; k++)
            {
                if (needle[k] != haystack[i + k]) break;
            }
            if (k == len) return i;
        }

        return -1;
    }

    static T[] SubArray<T>(this T[] data, int index, int length)
    {
        T[] result = new T[length];
        Array.Copy(data, index, result, 0, length);
        return result;
    }
    #endregion
}
