﻿using UnityEngine;

public class ImageRotator : MonoBehaviour
{
    public Vector3 rotation;

	void Update ()
    {
        transform.Rotate(rotation * Time.deltaTime);
    }
}
