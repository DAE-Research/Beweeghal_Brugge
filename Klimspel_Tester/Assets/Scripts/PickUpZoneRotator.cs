﻿using UnityEngine;
using System.Collections;

public class PickUpZoneRotator : MonoBehaviour
{
    public Vector2 RotZ;

    float _rotZ;
    Vector3 _rotationVector;

    void Start ()
    {
        _rotZ = Random.Range(RotZ.x, RotZ.y);
        _rotationVector = new Vector3(0, 0, _rotZ);
	}
	
	void Update ()
    {
        transform.Rotate(_rotationVector * Time.deltaTime);
    }
}
