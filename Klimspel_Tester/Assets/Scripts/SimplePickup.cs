﻿/*
 * +--+--+--+--+--+--+--+--o--+--+--CLIMBING GAME--+--+--+--+--+--+--+--+--+--+--+
 * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--o--+--+--+--+--+--+--+--+
 * +--+--+--+--+--+--+--+--+--+- Howest DAE-Research -+--+--+--+--+--+--+--+--+--+
 * 
 * This behaviour script handles the logic of a simplified pickup that is projected
 * on the wall with an animation and that disappears when a player touches over it.
 * The CreateCollisionMask function creates the mask by drawing a filled circle 
 * on the position of the pickup in the tracking camera's space. In the update
 * function the mask is checked with the playermask to detect collision.
 * 
 * +--+--+--+--+--+--+--+--o--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 * +--+--+--+--+--+--+--+--+--+--+--+--+--o--+--+--+--+--+--+--+--+--+--+--+--+--+
 */

using UnityEngine;
using UnityEngine.UI;
using OpenCVForUnity;

public class SimplePickup : MonoBehaviour
{
    public GameObject UICircle;
    public AudioClip PopSound;
    public AudioClip SpawnSound;
    public Mat Mask { get { return _mask; } private set { } }

    GameObject _hollowCircleObject;
    Mat _mask;
    bool _bPoppedUp = false;

    void Start()
    {
        PopUp();
        AudioSource.PlayClipAtPoint(SpawnSound, transform.position, 2.0f);

        CreateCollisionMask();
    }

    void OnDestroy()
    {
        if (_mask != null)
        {
            _mask.release();
        }
    }

    void Update()
    {
        // Check Collision
        if (_mask != null && CollisionHandler.Instance.CheckPlayerCollision(_mask))
        {
            TakePickUp();
        }
    }

    void PopUp()
    {
        if (!_bPoppedUp)
        {
            transform.Find("Sprite").GetComponent<Animator>().SetTrigger("PopUp");
            _bPoppedUp = true;

            _hollowCircleObject = Instantiate(UICircle);
            _hollowCircleObject.transform.SetParent(GameObject.FindGameObjectWithTag("CircleCanvas").transform);
            _hollowCircleObject.transform.position = gameObject.transform.position;
            _hollowCircleObject.GetComponent<Image>().fillAmount = 1;
        }
    }

    public void PopDown()
    {
        transform.Find("Sprite").GetComponent<Animator>().SetTrigger("PopDown");
        GetComponent<CircleCollider2D>().enabled = false;
    }

    public void TakePickUp()
    {
        PopDown();

        //play sound
        AudioSource.PlayClipAtPoint(PopSound, transform.position, 2.0f);

        Remove();
    }

    public void Remove()
    {
        if (_hollowCircleObject != null)
            Destroy(_hollowCircleObject);

        Destroy(gameObject);
    }

    public void CreateCollisionMask()
    {
        // Get the projected position onto the tracking camera's screen space
        var trackingCamera = GameObject.FindGameObjectWithTag("TrackingCamera").GetComponent<Camera>();
        var worldPosition = transform.position;
        worldPosition.y += worldPosition.y * Tracker.VerticalAdjustmentFactor;
        var projectedPosition = trackingCamera.WorldToViewportPoint(worldPosition);

        // Generate Mask with OpenCV
        _mask = new Mat(VapixHandler.FrameHeight, VapixHandler.FrameWidth, CvType.CV_8U);
        _mask.setTo(new Scalar(0, 0, 0));
        Point pixelPosition = new Point();
        pixelPosition.x = VapixHandler.FrameWidth * projectedPosition.x;
        pixelPosition.x += pixelPosition.x * 0.02f;
        pixelPosition.y = VapixHandler.FrameHeight * (1 - projectedPosition.y);

        double angle = 0;
        Imgproc.ellipse(_mask, pixelPosition, new Size(VapixHandler.FrameWidth * 0.007, VapixHandler.FrameHeight * 0.016), angle, angle, angle + 360, new Scalar(255, 255, 255), -1);

        Texture2D texture = new Texture2D(VapixHandler.FrameWidth, VapixHandler.FrameHeight);
        Utils.matToTexture2D(_mask, texture);

        // Visualize the mask
        var triggerMask = GameObject.Find("Mask_Trigger").GetComponent<Image>();
        Sprite sprite = Sprite.Create(texture, new UnityEngine.Rect(0, 0, VapixHandler.FrameWidth, VapixHandler.FrameHeight), new Vector2(0.5f, 0.5f));

        triggerMask.sprite = sprite;
    }
}
