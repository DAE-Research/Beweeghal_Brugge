﻿/*
 * +--+--+--+--+--+--+--+--o--+--+--CLIMBING GAME--+--+--+--+--+--+--+--+--+--+--+
 * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--o--+--+--+--+--+--+--+--+
 * +--+--+--+--+--+--+--+--+--+- Howest DAE-Research -+--+--+--+--+--+--+--+--+--+
 * 
 * This script was used to setup the mirror of the projector. This script however 
 * only works if the Qlc+ app on the server pc is running in web enabled mode.
 * It is no longer used since the Medialon(Ocular) is in place.
 * 
 * +--+--+--+--+--+--+--+--o--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 * +--+--+--+--+--+--+--+--+--+--+--+--+--o--+--+--+--+--+--+--+--+--+--+--+--+--+
 */

using UnityEngine;
using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using Helpers;

public class QLCPlusController : Singleton<QLCPlusController>
{
    string _host = "ws://172.21.224.23:9999/qlcplusWS";

    Dictionary<int, int> _preset = new Dictionary<int, int>()
    {
        { 1, 255 },
        { 2, 0 },
        { 3, 255 },
        { 4, 22 },
        { 5, 0 },
        { 6, 178 },
        { 7, 27 },
        { 8, 255 },
        { 9, 255 },
        { 10, 255 },
        { 11, 126 },
        { 12, 255 },
        { 13, 19 },
        { 14, 201 },
    };

    protected override void Awake()
    {
        base.Awake();
        DontDestroyOnLoad(gameObject);
    }

    public void Setup()
    {
        StartCoroutine(ActivatePreset());
    }

    IEnumerator ActivatePreset()
    {
        WebSocket w = new WebSocket(new Uri(_host));
        yield return StartCoroutine(w.Connect());

        foreach (var channel in _preset)
        {
            w.SendString("CH|" + channel.Key + "|" + channel.Value);
        }

        w.Close();
    }
}
