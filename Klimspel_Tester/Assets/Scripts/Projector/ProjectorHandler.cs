﻿/*
 * +--+--+--+--+--+--+--+--o--+--+--CLIMBING GAME--+--+--+--+--+--+--+--+--+--+--+
 * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--o--+--+--+--+--+--+--+--+
 * +--+--+--+--+--+--+--+--+--+- Howest DAE-Research -+--+--+--+--+--+--+--+--+--+
 * 
 * This script was used to access the basic functionalities of the projector, 
 * switching it on or of. It is no longer used since the Medialon(Ocular) is in
 * place.
 * 
 * +--+--+--+--+--+--+--+--o--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 * +--+--+--+--+--+--+--+--+--+--+--+--+--o--+--+--+--+--+--+--+--+--+--+--+--+--+
 */

using UnityEngine;

public class ProjectorHandler : MonoBehaviour
{
    string _ip = "172.21.224.25";
    HTTPRequestHandler _hr = new HTTPRequestHandler();
   
    public void Start()
    {
        _hr.Server = _ip;
    }

    public void SwitchLamp()
    {
        HTTPRequest request = new HTTPRequest();
        request.File = "projectorcontrol";
        request.Data.Add("request", "remote");
        request.Data.Add("id", "Lamp");
        _hr.SendHTTPRequest(request);
    }

    public void SwitchPattern()
    {
        HTTPRequest request = new HTTPRequest();
        request.File = "projectorcontrol";
        request.Data.Add("request", "remote");
        request.Data.Add("id", "Pattern");
        _hr.SendHTTPRequest(request);
    }
}
