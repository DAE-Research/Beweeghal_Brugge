# Klimspel Beweeghal Brugge

Om een spel te ontwikkelen voor de Howest Beweeghal in Brugge kan gestart worden van het Klimspel_Tester project.
Dit is een Unity project (32bit) en maakt gebruik van de projector en een camera in de beweeghal.
Wanneer het project wordt gespeeld verschijnen er pickups op de muur die verdwijnen wanneer een speler ze aanraakt.
Om het spel te kunnen testen is het belangrijk om uiteraard de projector en camera te activeren. Dit kan via de Medialon op de computer in de beweeghal.

Meer uitleg en informatie over het project is te vinden in de documentatie pdf.
